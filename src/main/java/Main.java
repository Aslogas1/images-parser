import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.URL;

public class Main {

    static String folderPath = "src\\main\\resources\\Images\\";

    public static void main(String[] args) {
        try {
            Document doc = Jsoup.connect("https://lenta.ru").get();
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                String src = element.absUrl("src");
                System.out.println(src);
                getImages(src);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void getImages(String src) throws IOException {
        int indexname = src.lastIndexOf("/");
        if (indexname == src.length()) {
            src = src.substring(1, indexname);
        }
        indexname = src.lastIndexOf("/");
        String name = src.substring(indexname);
        System.out.println(name);
        URL url = new URL(src);
        InputStream in = url.openStream();
        OutputStream out = new BufferedOutputStream(new FileOutputStream(folderPath + name));
        for (int b; (b = in.read()) != -1; ) {
            out.write(b);
        }
        out.close();
        in.close();

    }
}
